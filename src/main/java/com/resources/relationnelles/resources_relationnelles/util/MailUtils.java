package com.resources.relationnelles.resources_relationnelles.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailUtils {
	
	private static final String MDP_SENDER = "baguette";
	private static final String EMAIL_SENDER = "ressources.relationnelles.cesi@gmail.com";

	public static void sendMail(String recipeint,String digit) {
		Properties props = new Properties();
		  props.put("mail.smtp.starttls.enable", "true"); 
	        
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.user", EMAIL_SENDER); // User name
	        props.put("mail.smtp.password", MDP_SENDER); // password
	        props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");

	        Session session = Session.getInstance(props,
	         new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	               return new PasswordAuthentication(EMAIL_SENDER, MDP_SENDER);
	            }
	         });

		Message message = prepareMessage(session, EMAIL_SENDER, recipeint,digit);
		try {
			Transport.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	private static Message prepareMessage(Session session,String myAccountEmail, String to,String digit) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountEmail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Code de vérification");
			message.setContent("Le code de verification à noter est : </br></br>"+digit+"</br></br>"
					+ "   Ce mail est généré automatiquement. Veuiller ne pas y répondre.","text/html");
			return message;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
