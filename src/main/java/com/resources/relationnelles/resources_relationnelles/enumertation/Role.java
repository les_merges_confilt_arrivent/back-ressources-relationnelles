package com.resources.relationnelles.resources_relationnelles.enumertation;

public enum Role {
    ADMIN,
    MODERATEUR,
    SUPERADMIN,
	CITOYEN;
}
