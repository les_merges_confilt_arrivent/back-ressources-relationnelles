package com.resources.relationnelles.resources_relationnelles.controller;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.resources.relationnelles.resources_relationnelles.enumertation.Role;
import com.resources.relationnelles.resources_relationnelles.util.MailUtils;
import com.ressources.relationnelles.ressources_relationnelles.bean.BeanUtilisateur;
import com.ressources.relationnelles.ressources_relationnelles.service.UtilisateurService;


@Service
@Transactional
@RestController
@CrossOrigin(origins = "*")
public class PersonneController {
	
	@Autowired
	private UtilisateurService userService;

	@GetMapping("/api/createUtilisateur/{nom}/{prenom}/{email}/{mdp}")
	public Map<String, String> createNewCitoyen(@PathVariable String nom,@PathVariable String prenom,@PathVariable String email,@PathVariable String mdp) {
	    BeanUtilisateur beanUtilisateur = new BeanUtilisateur();
	    beanUtilisateur.setRole(Role.CITOYEN);
	    beanUtilisateur.setEmail(email);
	    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(mdp);
	    beanUtilisateur.setMdp(encode);
	    beanUtilisateur.setNom(nom);
	    beanUtilisateur.setPrenom(prenom);
	    userService.saveUtilisateur(beanUtilisateur);
	    return Collections.singletonMap("response", beanUtilisateur.getId().toString());
	}
	
//   @RequestMapping(value = "/api/login/{email}/{mdp}", produces = MediaType.APPLICATION_JSON_VALUE)
//	public Map<String, Object>  login(@PathVariable String email,@PathVariable String mdp) {
//		String toReturn = "non";
//		System.out.println(email+"/"+mdp);
//	    for (BeanUtilisateur user : userService.getUtilisateurs()) {	    	
//			if(user.getEmail().equals(email) && user.getMdp().equals(mdp)) {
//				return Collections.singletonMap("response", user.getId());
//			}
//		}
//		return Collections.singletonMap("response", toReturn);
//	}
   
   @RequestMapping(value ="api/login/mdp/forgot/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
   public Map mdpForgot(@PathVariable String email) {
	   for (BeanUtilisateur user : userService.getUtilisateurs()) {	    	
			if(user.getEmail().equals(email) ) {
			   	String verificationCode = getRandomNumberString();
			   	MailUtils.sendMail(email, verificationCode);
			    Map<String, String> map = new HashMap<>();
			    map.put("code", verificationCode);
			    map.put("iduser", String.valueOf(user.getId()));
				return map;
			}
		}
	   return Collections.singletonMap("error", "true");

   }
   
   @RequestMapping(value="api/login/mdp/change/{id}/{newMdp}", produces = MediaType.APPLICATION_JSON_VALUE)
   public Map changeMdp(@PathVariable Integer id,@PathVariable String newMdp) {
	   for (BeanUtilisateur user : userService.getUtilisateurs()) {
		   if(user.getId() == id ) {
			   	user.setMdp(newMdp);
			   	userService.saveUtilisateur(user);
				return Collections.singletonMap("response", id);
		   }
	   }
	   return null;
   }
   
   public static String getRandomNumberString() {
	    Random rnd = new Random();
	    int number = rnd.nextInt(999999);
	    return String.format("%06d", number);
	}

}
