package com.resources.relationnelles.resources_relationnelles.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ressources.relationnelles.ressources_relationnelles.security.JwtRequest;
import com.ressources.relationnelles.ressources_relationnelles.security.JwtTokenUtil;
import com.ressources.relationnelles.ressources_relationnelles.security.JwtUserDetailsService;
import com.ressources.relationnelles.ressources_relationnelles.service.UtilisateurService;


@RestController
@CrossOrigin
public class JwtAuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;
	
	@Autowired
	private UtilisateurService userService;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/authenticate/{email}/{mdp}", method = RequestMethod.POST)
	public Map createAuthenticationToken(@PathVariable  String email,@PathVariable String mdp) throws Exception {
		Map toReturn = new HashMap<String, Object>();
		JwtRequest authenticationRequest = new JwtRequest(email,mdp);
		
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);
		toReturn.put("token", token);
		toReturn.put("id", userService.getUtilisateurs().stream().filter(e -> e.getEmail().equals(email)).collect(Collectors.toList()).get(0).getId());

		return toReturn;
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}