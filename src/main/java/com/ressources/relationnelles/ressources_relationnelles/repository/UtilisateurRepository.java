package com.ressources.relationnelles.ressources_relationnelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanUtilisateur;

@Repository
public interface UtilisateurRepository extends JpaRepository<BeanUtilisateur, Long>{

}
