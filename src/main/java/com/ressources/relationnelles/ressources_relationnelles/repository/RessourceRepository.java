package com.ressources.relationnelles.ressources_relationnelles.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanRessource;

public interface RessourceRepository extends JpaRepository<BeanRessource, Long>{

}
