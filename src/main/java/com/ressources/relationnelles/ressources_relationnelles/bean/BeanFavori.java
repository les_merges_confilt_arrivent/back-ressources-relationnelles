package com.ressources.relationnelles.ressources_relationnelles.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="favori")
public class BeanFavori extends AbstractID{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4151571981417292015L;
	
	@Id
	@Column(name="id_favori")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@OneToOne
	@JoinColumn(name = "id_utilisateur", referencedColumnName = "idUtilisateur")
	private BeanUtilisateur utilisateur;
	@OneToOne
	@JoinColumn(name = "id_Ressource", referencedColumnName = "id_ressource")
	private BeanRessource ressource;

	
	
	
	
	public BeanFavori() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BeanFavori(Integer id, BeanUtilisateur utilisateur, BeanRessource ressource) {
		super();
		this.id = id;
		this.utilisateur = utilisateur;
		this.ressource = ressource;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BeanUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(BeanUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public BeanRessource getRessource() {
		return ressource;
	}

	public void setRessource(BeanRessource ressource) {
		this.ressource = ressource;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}



	