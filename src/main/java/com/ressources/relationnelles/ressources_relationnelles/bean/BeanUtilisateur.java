package com.ressources.relationnelles.ressources_relationnelles.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import com.resources.relationnelles.resources_relationnelles.enumertation.Role;


@Entity
@Table(name="utilisateur")
public class BeanUtilisateur extends AbstractID{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5184850966287495772L;
	@Id
	@Column(name="idUtilisateur")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="RoleUtilisateur")
	@Enumerated(EnumType.STRING)
	private Role role;
	@Column(name="NomUtilisateur")
	private String nom;
	@Column(name="PrenomUtilisateur")
	private String prenom;
	@Column(name="EmailUtilisateur")
	private String email;
	@Column(name="MdpUtilisateur")
	private String mdp;
	
	
	
	public BeanUtilisateur() {
		super();
	}
	public BeanUtilisateur(Integer id, Role role, String nom, String prenom, String email, String mdp) {
		super();
		this.id = id;
		this.role = role;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.mdp = mdp;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
