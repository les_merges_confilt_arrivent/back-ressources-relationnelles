package com.ressources.relationnelles.ressources_relationnelles.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="explore")
public class BeanExplore extends AbstractID{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2385616259832529825L;
	@Id
	@Column(name="id_explore")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@OneToOne
	@JoinColumn(name = "id_utilisateur", referencedColumnName = "idUtilisateur")
	private BeanUtilisateur utilisateur;
	@OneToOne
	@JoinColumn(name = "id_recherche", referencedColumnName = "idRecherche")
	private BeanRecherche recherche;
	
	
	

	public BeanExplore() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BeanExplore(Integer id, BeanUtilisateur utilisateur, BeanRecherche recherche) {
		super();
		this.id = id;
		this.utilisateur = utilisateur;
		this.recherche = recherche;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BeanUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(BeanUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public BeanRecherche getRecherche() {
		return recherche;
	}

	public void setRecherche(BeanRecherche recherche) {
		this.recherche = recherche;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
