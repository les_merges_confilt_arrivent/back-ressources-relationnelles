package com.ressources.relationnelles.ressources_relationnelles.bean;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings({ "rawtypes" })
public abstract class AbstractID implements Comparable, Serializable {
	/**
	 * 
	 */
	private static Logger logger = LogManager.getLogger(AbstractID.class);
	private static final long serialVersionUID = -7684428765381115874L;
	// MAPPING COLONE SQL

	public static final String USER = "user";
	public static final String DERNIEREMODIF = "dernieremodif";
	public static final String DATECREATION = "datecreation";
	public static final String SUPPRIMEE = "supprimee";
	public static final String JOUR = "day";
	public static final String USERDERNIERMODIF = "userdernieremodif";
	protected Integer id;
	protected String user;
	protected LocalDateTime dernieremodif;
	protected LocalDateTime datecreation;
	protected String userDernieremodif;
	protected String code;
	protected LocalDate dateDebActive;
	protected LocalDate dateFinActive;
	protected String active;

	public List<String> getAuditedFields() {
		return null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(final Integer id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(final String utilisateur) {
		this.user = utilisateur;
	}

	public LocalDateTime getDernieremodif() {
		return dernieremodif;
	}

	public void setDernieremodif(final LocalDateTime derniereModif) {
		this.dernieremodif = derniereModif;
	}

	public LocalDateTime getDatecreation() {
		return datecreation;
	}

	public void setDatecreation(final LocalDateTime dateCreation) {
		this.datecreation = dateCreation;
	}

	public String getUserDernieremodif() {
		return userDernieremodif;
	}

	public void setUserDernieremodif(String userDernieremodif) {
		this.userDernieremodif = userDernieremodif;
	}

	@Override
	public int compareTo(Object arg0) {
		AbstractID o = (AbstractID) arg0;
		return getId().compareTo(o.getId());
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDate getDateDebActive() {
		return dateDebActive;
	}

	public void setDateDebActive(LocalDate dateDebActive) {
		this.dateDebActive = dateDebActive;
	}

	public LocalDate getDateFinActive() {
		return dateFinActive;
	}

	public void setDateFinActive(LocalDate dateFinActive) {
		this.dateFinActive = dateFinActive;
	}

	public abstract boolean equals(Object o);

	public abstract int hashCode();



}
