package com.ressources.relationnelles.ressources_relationnelles.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="recherche")
public class BeanRecherche extends AbstractID{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -1908085056522772077L;

	@Id
	@Column(name="idRecherche")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="Contenu_Recherche")
	private String contenuRecherche;
	
	
	public BeanRecherche() {
	}
	public BeanRecherche(Integer id, String contenuRecherche) {
		super();
		this.id = id;
		this.contenuRecherche = contenuRecherche;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContenuRecherche() {
		return contenuRecherche;
	}
	public void setContenuRecherche(String contenuRecherche) {
		this.contenuRecherche = contenuRecherche;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
