package com.ressources.relationnelles.ressources_relationnelles.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="commentaire")
public class BeanCommentaire extends AbstractID{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="id_Commentaire")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="Contenu_Commentaire")
	private String contenu;
	@OneToOne
	@JoinColumn(name = "id_utilisateur", referencedColumnName = "idUtilisateur")
	private BeanUtilisateur utilisateur;
	@OneToOne
	@JoinColumn(name = "id_ressource", referencedColumnName = "id_ressource")
	private BeanRessource ressource;
	
	
	
	
	
	public BeanCommentaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BeanCommentaire(Integer id, String contenu, BeanUtilisateur utilisateur, BeanRessource ressource) {
		super();
		this.id = id;
		this.contenu = contenu;
		this.utilisateur = utilisateur;
		this.ressource = ressource;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public BeanUtilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(BeanUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	public BeanRessource getRessource() {
		return ressource;
	}
	public void setRessource(BeanRessource ressource) {
		this.ressource = ressource;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
