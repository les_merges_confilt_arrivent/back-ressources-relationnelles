package com.ressources.relationnelles.ressources_relationnelles.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="ressource")
public class BeanRessource extends AbstractID{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2771733783721555181L;

	
	@Id
	@Column(name="id_ressource")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="Valeur_Ressource")
	private String valeur;
	@Column(name="Type_Ressource")
	private String type;
	@Column(name="Nombre_Visite")
	private Integer nombreVisite;
	@ManyToOne
	@JoinColumn(name = "ID_Utilisateur", referencedColumnName = "idUtilisateur")
	private BeanUtilisateur utilisateur;
	
	
	
	public BeanRessource() {
		super();
	}

	public BeanRessource(Integer id, String valeur, String type, Integer nombreVisite, BeanUtilisateur utilisateur) {
		super();
		this.id = id;
		this.valeur = valeur;
		this.type = type;
		this.nombreVisite = nombreVisite;
		this.utilisateur = utilisateur;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValeur() {
		return valeur;
	}

	public void setValeur(String valeur) {
		this.valeur = valeur;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getNombreVisite() {
		return nombreVisite;
	}

	public void setNombreVisite(Integer nombreVisite) {
		this.nombreVisite = nombreVisite;
	}

	public BeanUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(BeanUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
