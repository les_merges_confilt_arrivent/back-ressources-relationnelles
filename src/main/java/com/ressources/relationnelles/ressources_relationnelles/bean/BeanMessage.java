package com.ressources.relationnelles.ressources_relationnelles.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="message")
public class BeanMessage extends AbstractID{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5890664257566033746L;
	@Id
	@Column(name="Id_Message")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	@Column(name="Contenu_Message")
	private String contenu;
	@Column(name="Date_Message")
	private Date dateMessage;
	@OneToOne
	@JoinColumn(name = "ID_Utilisateur", referencedColumnName = "idUtilisateur")
	private BeanUtilisateur utilisateur;
	
	
	
	
	public BeanMessage() {
		super();
	}
	public BeanMessage(Integer id, String contenu, Date dateMessage,BeanUtilisateur utilisateur) {
		super();
		this.id = id;
		this.contenu = contenu;
		this.dateMessage = dateMessage;
		this.utilisateur = utilisateur;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	public Date getDateMessage() {
		return dateMessage;
	}
	public void setDateMessage(Date dateMessage) {
		this.dateMessage = dateMessage;
	}
	public BeanUtilisateur getUtilisateur() {
		return utilisateur;
	}
	public void setUtilisateur(BeanUtilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}
}





