package com.ressources.relationnelles.ressources_relationnelles.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanUtilisateur;
import com.ressources.relationnelles.ressources_relationnelles.service.UtilisateurService;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UtilisateurService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println(username);
		List<BeanUtilisateur> user =  userService.getUtilisateurs().stream().filter(e -> e.getEmail().equals(username)).collect(Collectors.toList());
		
		System.out.println(user.get(0).getEmail());
		if(user.size() != 0 && user.get(0) != null) {
			return new User(user.get(0).getEmail(),user.get(0).getMdp(),new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
	}
}
