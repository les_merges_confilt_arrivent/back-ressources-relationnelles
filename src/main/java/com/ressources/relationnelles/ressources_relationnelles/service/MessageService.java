package com.ressources.relationnelles.ressources_relationnelles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanMessage;
import com.ressources.relationnelles.ressources_relationnelles.repository.MessageRepository;

@Service
public class MessageService {
    @Autowired
    private MessageRepository repository;

    public BeanMessage saveMessage(BeanMessage utilisateur) {
        return repository.save(utilisateur);
    }

    public List<BeanMessage> saveMessages(List<BeanMessage> utilisateurs) {
        return repository.saveAll(utilisateurs);
    }

    public List<BeanMessage> getMessages() {
        return repository.findAll();
    }

    public BeanMessage getMessageById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteMessage(Long id) {
        repository.deleteById(id);
        return "product removed !! " + id;
    }
    
}



