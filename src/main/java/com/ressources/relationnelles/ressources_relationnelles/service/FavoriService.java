package com.ressources.relationnelles.ressources_relationnelles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanFavori;
import com.ressources.relationnelles.ressources_relationnelles.repository.FavoriRepository;
@Service
public class FavoriService {
    @Autowired
    private FavoriRepository repository;

    public BeanFavori saveMessage(BeanFavori utilisateur) {
        return repository.save(utilisateur);
    }

    public List<BeanFavori> saveBeanFavoris(List<BeanFavori> utilisateurs) {
        return repository.saveAll(utilisateurs);
    }

    public List<BeanFavori> getBeanFavoris() {
        return repository.findAll();
    }

    public BeanFavori getBeanFavoriById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteBeanFavori(Long id) {
        repository.deleteById(id);
        return "product removed !! " + id;
    }
}



