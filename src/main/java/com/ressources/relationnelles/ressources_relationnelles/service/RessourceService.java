package com.ressources.relationnelles.ressources_relationnelles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanRessource;
import com.ressources.relationnelles.ressources_relationnelles.repository.RessourceRepository;


@Service
public class RessourceService {
	@Autowired
    private RessourceRepository repository;

    public BeanRessource saveRessource(BeanRessource utilisateur) {
        return repository.save(utilisateur);
    }

    public List<BeanRessource> saveRessources(List<BeanRessource> utilisateurs) {
        return repository.saveAll(utilisateurs);
    }

    public List<BeanRessource> getRessources() {
        return repository.findAll();
    }

    public BeanRessource getRessourceById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteRessource(Long id) {
        repository.deleteById(id);
        return "product removed !! " + id;
    }

}
