package com.ressources.relationnelles.ressources_relationnelles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanExplore;
import com.ressources.relationnelles.ressources_relationnelles.repository.ExploreRepository;

@Service
public class ExploreService {
	@Autowired
	private ExploreRepository repository;

	public BeanExplore saveExplore(BeanExplore Explore) {
		return repository.save(Explore);
	}

	public List<BeanExplore> saveExplores(List<BeanExplore> Explores) {
		return repository.saveAll(Explores);
	}

	public List<BeanExplore> getExplores() {
		return repository.findAll();
	}

	public BeanExplore getExploreById(Long id) {
		return repository.findById(id).orElse(null);
	}

	public String deleteExplore(Long id) {
		repository.deleteById(id);
		return "product removed !! " + id;
	}

}
