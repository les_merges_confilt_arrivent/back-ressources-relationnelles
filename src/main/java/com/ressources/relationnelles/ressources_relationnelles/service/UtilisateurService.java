package com.ressources.relationnelles.ressources_relationnelles.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanUtilisateur;
import com.ressources.relationnelles.ressources_relationnelles.repository.UtilisateurRepository;

import java.util.List;

@Service
public class UtilisateurService{
    @Autowired
    private UtilisateurRepository repository;

    public BeanUtilisateur saveUtilisateur(BeanUtilisateur utilisateur) {
        return repository.save(utilisateur);
    }

    public List<BeanUtilisateur> saveUtilisateurs(List<BeanUtilisateur> utilisateurs) {
        return repository.saveAll(utilisateurs);
    }

    public List<BeanUtilisateur> getUtilisateurs() {
        return repository.findAll();
    }

    public BeanUtilisateur getUtilisateurById(Long id) {
        return repository.getById(id);
    }

    public String deleteUtilisateur(Long id) {
        repository.deleteById(id);
        return "product removed !! " + id;
    }



}