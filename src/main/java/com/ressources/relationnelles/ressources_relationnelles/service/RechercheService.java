package com.ressources.relationnelles.ressources_relationnelles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanRecherche;
import com.ressources.relationnelles.ressources_relationnelles.bean.BeanUtilisateur;
import com.ressources.relationnelles.ressources_relationnelles.repository.RechercheRepository;


@Service
public class RechercheService{
    @Autowired
    private RechercheRepository repository;

    public BeanRecherche saveRecherche(BeanRecherche utilisateur) {
        return repository.save(utilisateur);
    }

    public List<BeanRecherche> saveRecherches(List<BeanRecherche> utilisateurs) {
        return repository.saveAll(utilisateurs);
    }

    public List<BeanRecherche> getRecherches() {
        return repository.findAll();
    }

    public BeanRecherche getRechercheById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public String deleteRecherche(Long id) {
        repository.deleteById(id);
        return "product removed !! " + id;
    }



}
