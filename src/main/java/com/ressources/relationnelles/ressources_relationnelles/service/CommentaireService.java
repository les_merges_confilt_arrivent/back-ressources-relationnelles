package com.ressources.relationnelles.ressources_relationnelles.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ressources.relationnelles.ressources_relationnelles.bean.BeanCommentaire;
import com.ressources.relationnelles.ressources_relationnelles.repository.CommentaireRepository;


@Service
public class CommentaireService {
	 @Autowired
	    private CommentaireRepository repository;

	    public BeanCommentaire saveCommentaire(BeanCommentaire utilisateur) {
	        return repository.save(utilisateur);
	    }

	    public List<BeanCommentaire> saveBeanCommentaires(List<BeanCommentaire> utilisateurs) {
	        return repository.saveAll(utilisateurs);
	    }

	    public List<BeanCommentaire> getBeanCommentaires() {
	        return repository.findAll();
	    }

	    public BeanCommentaire getBeanCommentaireById(Long id) {
	        return repository.findById(id).orElse(null);
	    }

	    public String deleteBeanCommentaire(Long id) {
	        repository.deleteById(id);
	        return "product removed !! " + id;
	    }
}
