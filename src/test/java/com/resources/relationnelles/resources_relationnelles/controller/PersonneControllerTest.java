package com.resources.relationnelles.resources_relationnelles.controller;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ressources.relationnelles.ressources_relationnelles.service.UtilisateurService;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class PersonneControllerTest {
	@Mock
	private UtilisateurService userService;
	@InjectMocks
	@Autowired
	private PersonneController personneController;
	
	private String code = null;
	private Integer idUser = null;
	


	@Test
	public void testCreateNewCitoyen() {
		System.out.println();
		Map returnJson = personneController.createNewCitoyen("testNom", "testPrenom", "test.test@test.com", "123");
		assertTrue(returnJson.get("response") != null);
	}

	@Test
	public void testLoginTrue() {
		Map returnJson = personneController.login("test.test@test.com", "123");
		System.out.println(returnJson.get("response"));
		assertTrue(returnJson.get("response") != "non");
	}
	
	@Test
	public void testLoginFalse() {
		Map returnJson = personneController.login("test.test@test.com", "1234");
		System.out.println(returnJson.get("response"));
		assertFalse(returnJson.get("response") != "non");
	}

	@Test
	public void testMdpForgot() {
		Map returnJson = personneController.mdpForgot("test.test@test.com");
		if(returnJson.get("code") != null) {
			code = returnJson.get("code").toString();
		}
		if(returnJson.get("code") != null) {
			idUser = Integer.parseInt(returnJson.get("iduser").toString());
		}
		assertTrue(returnJson.get("code") != null && returnJson.get("iduser")!= null);
	}

	@Test
	public void testChangeMdp() {
		Map returnJson = personneController.changeMdp(idUser, code);
		assertTrue(returnJson.get("response") != null);
	}

	@Test
	public void testGetRandomNumberString() {
		assertTrue(personneController.getRandomNumberString().length() == 6);
	}

}
